#!/usr/bin/env perl
use strict;
use warnings;
use File::Path qw(make_path);


my $git_dir = shift(@ARGV) // die("supply git_dir");
my $src_dir = shift(@ARGV) // die("supply src_dir");
die("$src_dir does not exist") unless(-d $src_dir);


$git_dir =~ s|/*$||;
$src_dir =~ s|/*$||;


sub call_git {
    my $git_dir=shift // die("supply git_dir");
    my $cmd=shift     // die("supply cmd");
    
    system(qq/env GIT_DIR="$git_dir" git $cmd/) == 0
	|| exit("cannot git $cmd");
}


# curried handle for increased elegance
my $git = sub { call_git($git_dir, @_); };


if(! -f $git_dir."/config") {
    make_path($git_dir) || warn("cannot mkdir $git_dir");
    &$git("init -b main");
    &$git("config core.bare false");
}


chdir($src_dir) || die("cannot chdir $src_dir");

&$git("add .");

chomp(my @statuslines = `env GIT_DIR="$git_dir" git status --porcelain=1`);
my $msg = join(qq/ |||  /, @statuslines);

chomp(my $commitmsg = `env GIT_DIR="$git_dir" LANG=C git commit -q -m "$msg" 2>&1`);
if($? != 0 && $commitmsg !~ m/nothing to commit/) {
    die("commit failed: $commitmsg");
}
